package com.luoyx.vjsb.admin;

import com.luoyx.vjsb.common.util.AesUtil;
import lombok.SneakyThrows;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/23 9:27
 */
public class PasswordTest {

    @SneakyThrows
    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String admin = bCryptPasswordEncoder.encode(AesUtil.aesEncrypt("123456"));
        System.err.println(admin);
        System.err.println(bCryptPasswordEncoder.matches("admin", admin));
    }
}
