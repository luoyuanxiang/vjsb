package com.luoyx.vjsb.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/14 10:42
 */
public class YaSuoTest {

    public static void main(String[] args) throws Exception {
        findpwd(10, "");
    }

    /**
     *
     * @param filePath 解压文件路径
     * @param targetPath 解压之后存放的路径
     * @param passwd 密码
     * @return
     * @throws Exception
     */
    public static int unrar(String filePath,String targetPath,String passwd) throws Exception {
        //winrar的执行路径
        String rarPath="C:\\Program Files\\WinRAR\\WinRAR.exe";
        StringBuilder sb = new StringBuilder();
        sb.append(rarPath);
        sb.append(" x -ibck -hp");
        sb.append(passwd).append(" -y ").append(filePath+" "+targetPath);
        Process process;

        process = Runtime.getRuntime().exec(sb.toString());
        if(process.waitFor() ==0 ){
            FileOutputStream fileOutputStream = new FileOutputStream(new File("D:\\test\\unrar\\getPassyewen4.txt"));
            String s = "解压后的密码："+passwd;
            fileOutputStream.write(s.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
            return 1;
        }else{
            System.out.println(new Date()+"--失败："+passwd);
            return 0 ;
        }
    }

    /**
     * 递归获取密码过程
     * @param deep 深度
     * @param parent 密码
     * @return
     * @throws Exception
     */
    public static int findpwd(int deep,String parent) throws Exception {
        String[] dir ={"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l",
            "z","x","c","v","b","n","m","Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X",
            "C","V","B","N","M","1","2","3","4","5","6","7","8","9","0","`","!","@","#","$","%","&","*","(",")","-","_","+","=","[",
            "]","{","}","|","\\","\"",":",";","\'","<",">",",",".","?","/"};
        if(deep == 1){
            if(unrar("D:\\Mind^Manager2020中文永久版.rar","D:\\MM2020中文永久版",parent) == 1){
                return 1;
            }else{
                return 0;
            }
        }else{
            for (String s : dir) {
                if (findpwd(deep - 1, parent + s) == 1) {
                    return 1;
                }
            }
        }
        return 0;
    }
}
