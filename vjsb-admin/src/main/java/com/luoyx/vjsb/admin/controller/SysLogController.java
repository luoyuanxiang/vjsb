package com.luoyx.vjsb.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luoyx.vjsb.common.query.CommonQuery;
import com.luoyx.vjsb.common.util.AjaxResult;
import com.luoyx.vjsb.core.entity.SysLog;
import com.luoyx.vjsb.core.service.ISysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/4/8 14:54
 */
@RestController
@RequestMapping("log")
@Api(tags = "日志")
public class SysLogController {

    @Resource
    ISysLogService iSysLogService;

    @PostMapping("list")
    @ApiOperation("获取分页日志")
    public AjaxResult<Page<SysLog>> list(CommonQuery query) {
        Page<SysLog> page = iSysLogService.page(new Page<>(query.getCurrent(), query.getSize()), new QueryWrapper<SysLog>().orderByDesc("create_time"));
        return AjaxResult.success(page);
    }

    @DeleteMapping("deleteLogs")
    @ApiOperation("删除日志")
    public AjaxResult<String> del(String[] ids) {
        return iSysLogService.removeByIds(Arrays.asList(ids)) ? AjaxResult.success() : AjaxResult.error();
    }
}
