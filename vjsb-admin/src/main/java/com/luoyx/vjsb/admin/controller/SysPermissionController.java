package com.luoyx.vjsb.admin.controller;


import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.luoyx.vjsb.authority.security.manager.MySecurityMetadataSource;
import com.luoyx.vjsb.authority.util.SecurityUtil;
import com.luoyx.vjsb.common.annotation.Logs;
import com.luoyx.vjsb.common.util.AjaxResult;
import com.luoyx.vjsb.core.entity.SysPermission;
import com.luoyx.vjsb.core.entity.SysRolePermission;
import com.luoyx.vjsb.core.entity.SysUser;
import com.luoyx.vjsb.core.service.ISysPermissionService;
import com.luoyx.vjsb.core.service.ISysRolePermissionService;
import com.luoyx.vjsb.core.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
@RestController
@RequestMapping("permission")
@Api(tags = "系统权限")
public class SysPermissionController {

    @Resource
    private ISysPermissionService iSysPermissionService;

    @Resource
    private ISysUserService iSysUserService;

    @Resource
    private ISysRolePermissionService iSysRolePermissionService;

    @Resource
    private SecurityUtil securityUtil;

    @Resource
    private MySecurityMetadataSource mySecurityMetadataSource;

    @PostMapping("asyncRoutes")
    public AjaxResult<List<SysPermission>> asyncRoutes() {
        List<SysPermission> permissions = iSysPermissionService.asyncRoutes(getRoleId(), 0);
        return AjaxResult.success(permissions);
    }

    @PostMapping("getMenu")
    @ApiOperation(value = "获取菜单Tree,全部")
    public AjaxResult<List<SysPermission>> getMenu() {
        List<SysPermission> permissions = iSysPermissionService.asyncRoutes(getRoleId(), null);
        return AjaxResult.success(permissions);
    }

    @GetMapping("findRoleIdByPermissionIds")
    @ApiOperation(value = "获取权限id")
    public AjaxResult<List<String>> findRoleIdByPermissionIds(String roleId) {
        List<SysRolePermission> list = iSysRolePermissionService.list(new QueryWrapper<SysRolePermission>().eq("role_id", roleId));
        List<String> ids = new ArrayList<>();
        list.forEach(sysRolePermission -> {
            ids.add(sysRolePermission.getPermissionId());
        });
        return AjaxResult.success(ids);
    }

    @PostMapping("addPermission")
    @ApiOperation(value = "添加菜单")
    @Logs(value = "添加菜单")
    public AjaxResult<String> add(@RequestBody SysPermission permission) {
        SysPermission one = iSysPermissionService.getOne(new QueryWrapper<SysPermission>().eq("level", permission.getLevel()).eq("title", permission.getTitle()));
        if (one != null) {
            return AjaxResult.error("权限名称不能重复");
        }
        permission.setCreateTime(DateUtil.date())
            .setUpdateTime(DateUtil.date());
        boolean save = iSysPermissionService.save(permission);
        return save ? AjaxResult.success("添加菜单成功") : AjaxResult.error("添加菜单出错");
    }

    @PatchMapping("updatePermission")
    @ApiOperation(value = "修改菜单")
    @Logs(value = "修改菜单")
    public AjaxResult<String> update(@RequestBody SysPermission permission) {
        permission.setUpdateTime(DateUtil.date());
        boolean b = iSysPermissionService.updateById(permission);
        return b ? AjaxResult.success("修改菜单成功") : AjaxResult.error("修改菜单失败");
    }

    @DeleteMapping("deletePermission")
    @ApiOperation(value = "删除菜单")
    @Logs(value = "删除菜单")
    public AjaxResult<String> del(String[] ids) {
        boolean b = iSysPermissionService.removeByIds(Arrays.asList(ids));
        if (b) {
            for (String id : ids) {
                iSysRolePermissionService.remove(new QueryWrapper<SysRolePermission>().eq("permission_id", id));
                mySecurityMetadataSource.loadResourceDefine();
            }
        }
        return b ? AjaxResult.success("删除菜单成功") : AjaxResult.error("删除菜单失败");
    }

    private String getRoleId() {
        SysUser u = securityUtil.getCurrUser();
        u.setPassword(null);
        return u.getRoleId();
    }

}
