package com.luoyx.vjsb.admin;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.net.InetAddress;

/**
 * <p>
 * 程序入口
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 10:25
 */
@Slf4j
@ServletComponentScan(basePackages = "com.luoyx.vjsb")
@SpringBootApplication(scanBasePackages = "com.luoyx.vjsb", exclude = {
    DruidDataSourceAutoConfigure.class,
    org.activiti.spring.boot.SecurityAutoConfiguration.class
})
public class VjsbAdminApplication {

    @SneakyThrows
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(VjsbAdminApplication.class, args);
        ConfigurableEnvironment environment = context.getEnvironment();
        log.info("\n***********************************************************\n" +
                "*\n" +
                "*\n" +
                "* 这个是啥！！！！！！！！！！！！！！！！！！！！！！！！！！！！启动成功！\n" +
                "*\n" +
                "*\n" +
                "***********************************************************\n" +
                "           _.._        ,------------------------.\n" +
                "        ,'      `.    ( 启动成功！开启学习之旅！   )\n" +
                "       /  __) __` \\    `-,----------------------'\n" +
                "      (  (`-`(-')  ) _.-'\n" +
                "      /)  \\  = /  (\n" +
                "     /'    |--' .  \\\n" +
                "    (  ,---|  `-.)__`\n" +
                "     )(  `-.,--'   _`-.\n" +
                "    '/,'          (  Uu\",\n" +
                "     (。       ,    `/,-' )\n" +
                "     `.__,  : `。'/  /`--'\n" +
                "       |     `--'  |\n" +
                "       `   `-._   /\n" +
                "        \\        (\n" +
                "        /\\ .      \\.\n" +
                "       / |` \\     ,-\\\n" +
                "      /  \\| .)   /   \\\n" +
                "     ( ,'|\\    ,'     :\n" +
                "     | \\,`.`--\"/      }\n" +
                "     `,'    \\  |,'    /\n" +
                "    / \"-._   `-/      |\n" +
                "    \"-.   \"-.,'|     ;\n" +
                "   /        _/[\"---'\"\"]\n" +
                "  :        /  |\"-     '\n" +
                "  '           |      /\n" +
                "  |           `      |" +
                "\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:{}\n\t" +
                "External: \thttp://{}:{}\n" +
                "----------------------------------------------------------",
            environment.getProperty("spring.application.name"),
            environment.getProperty("server.port"),
            InetAddress.getLocalHost().getHostAddress(),
            environment.getProperty("server.port"));
    }

}
