package com.luoyx.vjsb.admin.exception;

import com.luoyx.vjsb.common.exception.MyException;
import com.luoyx.vjsb.common.util.AjaxResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <p>
 * 异常处理
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/12 11:57
 */
@RestControllerAdvice
public class WebExceptionConfig {

    @ExceptionHandler(MyException.class)
    public AjaxResult<String> exception(MyException e) {
        return AjaxResult.success(e.getMessage(), 1007, null);
    }
}
