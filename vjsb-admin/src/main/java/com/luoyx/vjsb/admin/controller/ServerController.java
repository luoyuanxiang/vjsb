package com.luoyx.vjsb.admin.controller;

import com.luoyx.vjsb.common.util.AjaxResult;
import com.luoyx.vjsb.core.monitor.Server;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统服务
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/4/28 9:19
 */
@RestController
@RequestMapping("/monitor/server")
@PreAuthorize("hasRole('ROLE_supper_admin')")
public class ServerController {

    @GetMapping()
    public AjaxResult<Server> info() throws Exception {
        Server server = new Server();
        server.copyTo();
        return AjaxResult.success(server);
    }
}
