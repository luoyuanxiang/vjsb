package com.luoyx.vjsb.admin.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luoyx.vjsb.authority.util.SecurityUtil;
import com.luoyx.vjsb.authority.util.UserUtil;
import com.luoyx.vjsb.common.annotation.Logs;
import com.luoyx.vjsb.common.util.AesUtil;
import com.luoyx.vjsb.common.util.AjaxResult;
import com.luoyx.vjsb.core.entity.SysUser;
import com.luoyx.vjsb.core.query.UserQuery;
import com.luoyx.vjsb.core.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:56
 */
@RestController
@Api(tags = "系统用户")
@RequestMapping("user")
public class SysUserController {

    @Resource
    private ISysUserService iSysUserService;

    @Resource
    private SecurityUtil securityUtil;

    @PostMapping("list")
    @ApiOperation(value = "获取用户分页信息")
    public AjaxResult<IPage<SysUser>> list(UserQuery userQuery) {
        return AjaxResult.success(iSysUserService.page(userQuery));
    }


    @GetMapping("info")
    @ApiOperation("获取用户信息")
    public AjaxResult<SysUser> info() {
        SysUser u = securityUtil.getCurrUser();
        u.setPassword(null);
        u.setButtons(new ArrayList<>(Objects.requireNonNull(UserUtil.getPermissions(u))));
        u.setRoleName(new ArrayList<>(Objects.requireNonNull(UserUtil.getRoles(u))).get(0));
        return AjaxResult.success(u);
    }

    @PostMapping("add")
    @ApiOperation("添加用户信息")
    @Logs(value = "添加用户")
    public AjaxResult<String> add(@RequestBody SysUser user) {
        SysUser username = iSysUserService.getOne(new QueryWrapper<SysUser>().eq("username", user.getUsername()));
        if (username != null) {
            return AjaxResult.error("该用户已存在！");
        }
        user.setPassword(new BCryptPasswordEncoder().encode(AesUtil.aesEncrypt("123456")))
            .setCreateTime(DateUtil.date())
            .setUpdateTime(DateUtil.date());
        boolean save = iSysUserService.save(user);
        return save ? AjaxResult.success("添加用户成功") : AjaxResult.error("添加用户失败");
    }

    @PatchMapping("update")
    @ApiOperation(value = "修改用户")
    @Logs(value = "修改用户")
    public AjaxResult<String> update(@RequestBody SysUser user) {
        user.setUpdateTime(DateUtil.date());
        if (!"".equals(user.getPassword()) && user.getPassword() != null) {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        }
        boolean b = iSysUserService.updateById(user);
        return b ? AjaxResult.success("修改用户成功") : AjaxResult.error("修改用户失败");
    }

    @DeleteMapping("del")
    @ApiOperation(value = "删除用户")
    @Logs(value = "删除用户")
    public AjaxResult<String> del(String[] ids) {
        boolean b = iSysUserService.removeByIds(Arrays.asList(ids));
        return b ? AjaxResult.success("删除用户成功") : AjaxResult.error("删除用户失败");
    }

    @PutMapping("updatePassword")
    @Logs(value = "修改密码")
    @ApiOperation(value = "修改密码")
    public AjaxResult<String> updatePassword(String originalPassword, String newPassword) {
        SysUser currUser = securityUtil.getCurrUser();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (!bCryptPasswordEncoder.matches(originalPassword, currUser.getPassword())) {
            return AjaxResult.error("原密码输入错误");
        }
        currUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));
        iSysUserService.updateById(currUser);
        return AjaxResult.success();
    }
}
