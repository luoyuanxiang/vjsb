package com.luoyx.vjsb.core.query;

import com.luoyx.vjsb.common.query.CommonQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户查询
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/25 14:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户查询")
public class UserQuery extends CommonQuery {

    @ApiModelProperty(value = "是否禁用", example = "0")
    private Integer available;
}
