package com.luoyx.vjsb.core.mapper;

import com.luoyx.vjsb.core.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
