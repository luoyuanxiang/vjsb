package com.luoyx.vjsb.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.luoyx.vjsb.common.object.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:08
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "系统用户")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = -5578077206384397320L;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "是否禁用")
    private Boolean available;

    @ApiModelProperty(value = "登录次数", example = "1")
    private Integer loginCount;

    @ApiModelProperty(value = "最后一次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    @ApiModelProperty(value = "最后一次登录IP")
    private String lastLoginIp;

    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;

    @ApiModelProperty("权限按钮")
    @TableField(exist = false)
    private List<String> buttons;
}
