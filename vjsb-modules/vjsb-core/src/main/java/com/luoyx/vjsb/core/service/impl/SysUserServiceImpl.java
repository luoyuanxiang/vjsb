package com.luoyx.vjsb.core.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luoyx.vjsb.core.entity.SysUser;
import com.luoyx.vjsb.core.mapper.SysUserMapper;
import com.luoyx.vjsb.core.query.UserQuery;
import com.luoyx.vjsb.core.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:13
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    /**
     * 分页自定义查询
     *
     * @param userQuery 查询
     * @return 1
     */
    @Override
    public IPage<SysUser> page(UserQuery userQuery) {
        return baseMapper.page(new Page<>(userQuery.getCurrent(), userQuery.getSize()), userQuery);
    }
}
