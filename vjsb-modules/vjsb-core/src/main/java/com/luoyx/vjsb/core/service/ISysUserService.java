package com.luoyx.vjsb.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.luoyx.vjsb.core.entity.SysUser;
import com.luoyx.vjsb.core.query.UserQuery;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:12
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 分页自定义查询
     *
     * @param userQuery 查询
     * @return 1
     */
    IPage<SysUser> page(UserQuery userQuery);

}
