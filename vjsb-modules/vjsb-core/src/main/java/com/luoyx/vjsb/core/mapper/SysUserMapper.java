package com.luoyx.vjsb.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luoyx.vjsb.core.entity.SysUser;
import com.luoyx.vjsb.core.query.UserQuery;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统用户mapper
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:10
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 分页自定义查询
     *
     * @param page 1
     * @param userQuery 2
     * @return 3
     */
    IPage<SysUser> page(@Param("page") IPage<SysUser> page, @Param("userQuery") UserQuery userQuery);
}
