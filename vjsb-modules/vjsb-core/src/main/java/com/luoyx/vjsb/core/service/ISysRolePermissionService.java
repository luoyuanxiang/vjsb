package com.luoyx.vjsb.core.service;

import com.luoyx.vjsb.core.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
