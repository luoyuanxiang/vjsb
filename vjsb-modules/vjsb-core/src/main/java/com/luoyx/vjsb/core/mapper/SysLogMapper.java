package com.luoyx.vjsb.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luoyx.vjsb.core.entity.SysLog;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/20 11:29
 */
public interface SysLogMapper extends BaseMapper<SysLog> {
}
