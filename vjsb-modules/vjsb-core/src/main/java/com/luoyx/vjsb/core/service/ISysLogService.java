package com.luoyx.vjsb.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luoyx.vjsb.core.entity.SysLog;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/4/8 11:35
 */
public interface ISysLogService extends IService<SysLog> {
}
