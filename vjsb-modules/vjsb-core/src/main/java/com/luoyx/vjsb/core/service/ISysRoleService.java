package com.luoyx.vjsb.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luoyx.vjsb.core.entity.SysRole;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:12
 */
public interface ISysRoleService extends IService<SysRole> {
}
