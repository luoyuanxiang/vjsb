package com.luoyx.vjsb.core.service;

import com.luoyx.vjsb.core.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
public interface ISysPermissionService extends IService<SysPermission> {

    /**
     * 获取动态路由
     * @param roleId roleId
     * @param type 类型
     * @return 1
     */
    List<SysPermission> asyncRoutes(String roleId, Integer type);
}
