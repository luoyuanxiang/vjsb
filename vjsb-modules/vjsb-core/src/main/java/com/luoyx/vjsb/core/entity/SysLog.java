package com.luoyx.vjsb.core.entity;

import com.luoyx.vjsb.common.object.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 日志记录
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/20 11:12
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class SysLog extends BaseEntity {
    private static final long serialVersionUID = -2489484467200778753L;


    /**
     * 登录用户id
     */
    private String userName;

    /**
     * 日志级别 ERROR','WARN','INFO
     */
    private String logLevel;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 日志内容
     */
    private String content;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 操作用户的user_agent
     */
    private String ua;

    /**
     * 请求方法
     */
    private String requestMethod;

    /**
     * 消耗时间
     */
    private Long consumingTime;

    /**
     * 异常详情信息
     */
    private String exDetail;

    /**
     * 异常描述
     */
    private String exMsg;
}
