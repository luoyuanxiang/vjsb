package com.luoyx.vjsb.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.luoyx.vjsb.common.object.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 权限
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/26 13:52
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "系统权限")
public class SysPermission extends BaseEntity {
    private static final long serialVersionUID = 3699767858771900815L;

    @ApiModelProperty("菜单/权限名称")
    private String name;

    @ApiModelProperty("前端组件")
    private String component;

    @ApiModelProperty("路径")
    private String path;

    @ApiModelProperty("子级菜单")
    @TableField(exist = false)
    private List<SysPermission> children;

    @ApiModelProperty("父级id, -1顶级菜单")
    private String parentId;

    @ApiModelProperty(value = "层级", example = "1")
    private Integer level;

    @ApiModelProperty("权限,多个使用逗号分隔")
    private String permission;

    @ApiModelProperty(value = "类型 -1菜单 1页面 0按钮", example = "1")
    private Integer type;

    @ApiModelProperty("菜单名称")
    private String title;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty(value = "排序", example = "1")
    private Integer sort;

}
