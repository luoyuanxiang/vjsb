package com.luoyx.vjsb.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luoyx.vjsb.core.entity.SysPermission;
import com.luoyx.vjsb.core.entity.SysRolePermission;
import com.luoyx.vjsb.core.mapper.SysPermissionMapper;
import com.luoyx.vjsb.core.service.ISysPermissionService;
import com.luoyx.vjsb.core.service.ISysRolePermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    @Resource
    private ISysRolePermissionService iSysRolePermissionService;
    /**
     * 获取动态路由
     *
     * @param roleId roleId
     * @param type 类型
     * @return 1
     */
    @Override
    public List<SysPermission> asyncRoutes(String roleId, Integer type) {
        List<SysPermission> rootList;
        List<SysPermission> sysPermissions;
        List<SysPermission> list;
        if(roleId != null) {
            List<SysRolePermission> sysRolePermissions = iSysRolePermissionService.list(new QueryWrapper<SysRolePermission>().eq("role_id", roleId));
            List<String> ids = new ArrayList<>(sysRolePermissions.size());
            sysRolePermissions.forEach(sysRolePermission -> ids.add(sysRolePermission.getPermissionId()));
            sysPermissions = baseMapper.asyncRoutes(ids);
        } else {
            sysPermissions = baseMapper.asyncRoutes(null);
        }
        rootList = sysPermissions.stream()
            .filter(sysPermission -> "-1".equals(sysPermission.getParentId()))
            .collect(Collectors.toList());
        if(type == null) {
            list = sysPermissions.stream()
                .filter(sysPermission -> !"-1".equals(sysPermission.getParentId()))
                .collect(Collectors.toList());
        } else {
            list = sysPermissions.stream()
                .filter(sysPermission -> !"-1".equals(sysPermission.getParentId()) && 0 != sysPermission.getType())
                .collect(Collectors.toList());
        }
        rootList.forEach(root -> {
            Set<String> ids = new HashSet<>(list.size());
            getTree(root, list, ids);
        });
        return rootList;
    }

    private void getTree(SysPermission parent, List<SysPermission> rootList, Set<String> ids) {
        List<SysPermission> sysPermissions = new ArrayList<>();
        rootList.stream().filter(root -> !ids.contains(root.getId()))
                .filter(root -> root.getParentId().equals(parent.getId()))
                .forEach(root -> {
                    ids.add(root.getId());
                    getTree(root, rootList, ids);
                    sysPermissions.add(root);
                });
        parent.setChildren(sysPermissions);
    }
}
