package com.luoyx.vjsb.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luoyx.vjsb.core.entity.SysRole;

/**
 * <p>
 * 系统角色mapper
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:09
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
