package com.luoyx.vjsb.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.luoyx.vjsb.common.object.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/19 11:07
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "系统角色")
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = -236389600244216583L;
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色描述")
    private String roleDesc;

    @ApiModelProperty("权限id")
    @TableField(exist = false)
    private List<String> permissionIds;
}
