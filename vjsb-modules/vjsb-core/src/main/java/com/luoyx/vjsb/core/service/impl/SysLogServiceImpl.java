package com.luoyx.vjsb.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luoyx.vjsb.common.event.SysEvent;
import com.luoyx.vjsb.common.vo.SysLogVO;
import com.luoyx.vjsb.core.entity.SysLog;
import com.luoyx.vjsb.core.mapper.SysLogMapper;
import com.luoyx.vjsb.core.service.ISysLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/4/8 11:36
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {
    /**
     * 保存日志
     *
     * @param eventObject 1
     */
    @Async
    @Order
    @EventListener(SysEvent.class)
    public void saveSysLog(SysEvent eventObject) {
        SysLogVO source = (SysLogVO)eventObject.getSource();
        SysLog sysLog = new SysLog();
        BeanUtils.copyProperties(source, sysLog);
        this.save(sysLog);
    }
}
