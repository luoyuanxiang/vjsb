package com.luoyx.vjsb.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luoyx.vjsb.core.entity.SysPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020-03-26
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    /**
     * 获取菜单
     * @param ids 1
     * @return 1
     */
    List<SysPermission> asyncRoutes(@Param("ids") List<String> ids);
}
