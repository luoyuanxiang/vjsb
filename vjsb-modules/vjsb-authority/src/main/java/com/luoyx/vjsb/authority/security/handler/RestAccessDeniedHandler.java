package com.luoyx.vjsb.authority.security.handler;

import com.luoyx.vjsb.common.util.AjaxResult;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * 权限控制处理类
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 17:04
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
        throws IOException, ServletException {
        AjaxResult.out(response, AjaxResult.success("抱歉，您没有访问权限", 401, null));
    }
}
