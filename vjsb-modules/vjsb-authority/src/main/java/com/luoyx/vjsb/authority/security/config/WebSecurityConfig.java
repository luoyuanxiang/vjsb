package com.luoyx.vjsb.authority.security.config;

import com.luoyx.vjsb.authority.security.UserDetailsServiceImpl;
import com.luoyx.vjsb.authority.security.filter.JWTAuthenticationFilter;
import com.luoyx.vjsb.authority.security.handler.AuthenticationFailHandler;
import com.luoyx.vjsb.authority.security.handler.AuthenticationSuccessHandler;
import com.luoyx.vjsb.authority.security.handler.RestAccessDeniedHandler;
import com.luoyx.vjsb.authority.security.manager.MyFilterSecurityInterceptor;
import com.luoyx.vjsb.authority.security.properties.IgnoredUrlsProperties;
import com.luoyx.vjsb.authority.security.properties.TokenProperties;
import com.luoyx.vjsb.authority.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import javax.annotation.Resource;

/**
 * <p>
 * Security 核心配置类
 * 开启注解控制权限至Controller
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 17:28
 */
@Slf4j
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private TokenProperties tokenProperties;

    @Resource
    private IgnoredUrlsProperties ignoredUrlsProperties;

    @Resource
    private UserDetailsServiceImpl userDetailsService;

    @Resource
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Resource
    private AuthenticationFailHandler failHandler;

    @Resource
    private RestAccessDeniedHandler accessDeniedHandler;

    @Resource
    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private SecurityUtil securityUtil;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http
            .authorizeRequests();

        // 除配置文件忽略路径其它所有请求都需经过认证和授权
        ignoredUrlsProperties.getUrls().forEach(url -> registry.antMatchers(url).permitAll());

        registry.and()
            // 表单登录方式
            .formLogin()
            // 登录页面
            .loginPage("/api/needLogin")
            // 登录请求url
            .loginProcessingUrl("/api/login")
            .permitAll()
            // 成功处理类
            .successHandler(authenticationSuccessHandler)
            // 失败
            .failureHandler(failHandler)
            .and()
            // 允许网页iframe
            .headers().frameOptions().disable()
            .and()
            .logout()
            .permitAll()
            .and()
            .authorizeRequests()
            // 任何请求
            .anyRequest()
            // 需要身份认证
            .authenticated()
            .and()
            // 允许跨域
            .cors().and()
            // 关闭跨站请求防护
            .csrf().disable()
            // 前后端分离采用JWT 不需要session
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            // 自定义权限拒绝处理类
            .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
            .and()
            // 添加自定义权限过滤器
            .addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class)
            // 添加JWT认证过滤器
            .addFilter(new JWTAuthenticationFilter(authenticationManager(), tokenProperties, stringRedisTemplate, securityUtil));
    }
}
