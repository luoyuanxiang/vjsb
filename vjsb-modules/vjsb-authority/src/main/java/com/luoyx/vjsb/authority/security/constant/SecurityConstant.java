package com.luoyx.vjsb.authority.security.constant;

/**
 * <p>
 * 权限常量
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 16:31
 */
public interface SecurityConstant {

    /**
     * token分割
     */
    String TOKEN_SPLIT = "Bearer ";

    /**
     * JWT签名加密key
     */
    String JWT_SIGN_KEY = "vjsb";

    /**
     * token参数头
     */
    String HEADER = "Authorization";

    /**
     * 权限参数头
     */
    String AUTHORITIES = "authorities";

    /**
     * 用户选择JWT保存时间参数头
     */
    String SAVE_LOGIN = "saveLogin";

    /**
     * 交互token前缀key
     */
    String TOKEN_PRE = "VJSB_TOKEN_PRE:";

    /**
     * 用户token前缀key 单点登录使用
     */
    String USER_TOKEN = "VJSB_USER_TOKEN:";
}
