package com.luoyx.vjsb.authority.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 16:40
 */
@Data
@AllArgsConstructor
public class TokenUser {
    private String username;

    private List<String> permissions;

    private Boolean saveLogin;

}
