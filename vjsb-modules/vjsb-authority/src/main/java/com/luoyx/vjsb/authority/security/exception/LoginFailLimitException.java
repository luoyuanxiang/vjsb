package com.luoyx.vjsb.authority.security.exception;

import org.springframework.security.authentication.InternalAuthenticationServiceException;

/**
 * <p>
 * 登录错误异常
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 14:24
 */
public class LoginFailLimitException extends InternalAuthenticationServiceException {

    public LoginFailLimitException(String msg){
        super(msg);
    }

    public LoginFailLimitException(String msg, Throwable t) {
        super(msg, t);
    }
}
