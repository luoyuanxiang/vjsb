package com.luoyx.vjsb.activiti.vo;

import lombok.Data;

/**
 * <p>
 * 判断网关
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 17:01
 */
@Data
public class JudgeWayVO {

    /**
     * 是否通过 1-通过 0-不通过 -1回退
     */
    private Integer isPass;

    /**
     * 是否结束 1-结束 0-没有结束
     */
    private Integer isEnd;

    /**
     * 是否是会签任务 1-是 0-不是
     */
    private Integer isSign;
}
