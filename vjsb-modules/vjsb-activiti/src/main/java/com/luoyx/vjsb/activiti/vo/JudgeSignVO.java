package com.luoyx.vjsb.activiti.vo;

import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 17:08
 */
@Data
public class JudgeSignVO {

    /**
     * 任务id
     */
    private String taskId;

    /**
     * 是否是有权限 1-是 0-不是
     */
    private Integer isAuth;
}
