package com.luoyx.vjsb.activiti.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luoyx.vjsb.activiti.entity.TaskAssignee;
import com.luoyx.vjsb.activiti.service.IActivityService;
import com.luoyx.vjsb.activiti.service.ITaskAssigneeService;
import com.luoyx.vjsb.common.query.CommonQuery;
import com.luoyx.vjsb.common.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.activiti.rest.service.api.repository.ModelRequest;
import org.activiti.rest.service.api.repository.ModelResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 流程
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/14 14:13
 */
@Slf4j
@RestController
@RequestMapping("activiti")
@Api(tags = "流程模型管理")
public class ActivitiController {

    @Resource
    private RepositoryService repositoryService;

    @Resource
    private IActivityService iActivityService;

    @Resource
    private ITaskAssigneeService iTaskAssigneeService;


    /**
     * 获取所有模型
     *
     * @param query 查询条件
     * @return 模型
     */
    @PostMapping("/list")
    @ResponseBody
    @ApiOperation("获取分页模型")
    public AjaxResult<Page<Model>> list(CommonQuery query) {
        Page<Model> page = iActivityService.getPage(query);
        return AjaxResult.success(page);
    }

    /**
     * 创建模型
     *
     * @param name        模型名称
     * @param key         模型key
     * @param description 模型描述
     * @return 模型id
     */
    @PostMapping("create")
    @ApiOperation("创建模型")
    public AjaxResult<String> createModeler(@RequestParam("name") String name,
                                            @RequestParam("key") String key,
                                            @RequestParam("description") String description) {
        String id = iActivityService.createModeler(name, key, description);
        return id == null ? AjaxResult.error() : AjaxResult.success(id, "创建模型成功");
    }

    @GetMapping("models/{modelId}")
    public AjaxResult<String> source(@PathVariable("modelId") String id) {
        return AjaxResult.success();
    }

    @ApiOperation("修改模型")
    @PutMapping("update/{modelId}")
    public AjaxResult<ModelResponse> updateModel(@PathVariable("modelId") String id,
                                          @RequestBody ModelRequest modelRequest) {
        ModelResponse modelResponse = iActivityService.updateModel(id, modelRequest);
        return AjaxResult.success(modelResponse);
    }

    /**
     * 根据Model部署流程
     *
     * @param modelId 模型id
     * @return 1
     */
    @GetMapping("deploy/{modelId}")
    @ApiOperation("部署流程")
    public AjaxResult<String> deploy(@PathVariable("modelId") String modelId) {
        boolean b = iActivityService.deploy(modelId);
        return b ? AjaxResult.success("部署成功") : AjaxResult.error();
    }

    /**
     * 删除模型
     *
     * @param ids 模型id
     * @return 1
     */
    @DeleteMapping("/remove")
    @ResponseBody
    @ApiOperation("删除模型")
    public AjaxResult<String> remove(String[] ids) {
        try {
            for (String id : ids) {
                Model model = repositoryService.createModelQuery().modelId(id).singleResult();
                repositoryService.deleteModel(model.getId());
            }
            return AjaxResult.success();
        } catch (Exception e) {
            return AjaxResult.error();
        }
    }

    @PatchMapping("modelerStep")
    @ResponseBody
    public AjaxResult<String> modelerStep(String activityId, String roleId) {
        TaskAssignee taskAssignee = iTaskAssigneeService.getOne(new QueryWrapper<TaskAssignee>()
            .eq("activity_id", activityId));
        taskAssignee.setRoleId(roleId).setUpdateTime(DateUtil.date());
        boolean b = iTaskAssigneeService.updateById(taskAssignee);
        return b ? AjaxResult.success() : AjaxResult.error();
    }

}
