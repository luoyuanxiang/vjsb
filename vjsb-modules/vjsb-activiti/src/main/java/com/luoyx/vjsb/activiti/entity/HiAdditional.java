package com.luoyx.vjsb.activiti.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 流程历史表
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 17:02
 */
@Data
@TableName("act_hi_additional")
public class HiAdditional extends Model<HiAdditional> {

    /**
     * 主键
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 节点实例ID
     */
    @TableField("task_id_")
    private String taskId;
    /**
     * 节点实例名称
     */
    @TableField("task_name")
    private String taskName;
    /**
     * 是否是第一次提交
     */
    @TableField("is_first")
    private Integer isFirst;
    /**
     * 流程实例id
     */
    @TableField("pro_inst_id")
    private String proInstId;
    /**
     * 说明
     */
    private String remark;
    /**
     * 操作结果
     */
    private Integer result;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
}
