package com.luoyx.vjsb.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 模型对应节点的完成角色（自建）
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/2 8:58
 */
@Data
@Accessors(chain = true)
@TableName("act_task_assignee")
@EqualsAndHashCode(callSuper = true)
@ApiModel("模型对应节点的完成角色（自建）")
public class TaskAssignee extends Model<TaskAssignee> {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("模型id")
    private String modelId;

    @ApiModelProperty("部署id")
    private String deploymentId;

    @ApiModelProperty("节点id")
    private String activityId;

    @ApiModelProperty("角色id")
    private String roleId;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("修改时间")
    private Date updateTime;

}
