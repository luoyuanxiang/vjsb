package com.luoyx.vjsb.activiti.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 进程实例
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/4 11:29
 */
@Data
@Accessors(chain = true)
public class ProcessInstanceVO {

    /**
     * 进程实例id
     */
    private String processInstanceId;

    /**
     * 进程实例名称
     */
    private String processInstanceName;
}
