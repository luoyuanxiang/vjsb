package com.luoyx.vjsb.activiti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luoyx.vjsb.activiti.entity.TaskAssignee;
import com.luoyx.vjsb.activiti.mapper.TaskAssigneeMapper;
import com.luoyx.vjsb.activiti.service.ITaskAssigneeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/2 9:26
 */
@Service
public class TaskAssigneeServiceImpl extends ServiceImpl<TaskAssigneeMapper, TaskAssignee> implements ITaskAssigneeService {
    /**
     * 获取自定义委派人
     *
     * @param modelId    模型id
     * @param activityId 节点id
     * @return modelId
     */
    @Override
    public TaskAssignee queryByModelIdAndActivityId(String modelId, String activityId) {
        return getOne(new QueryWrapper<TaskAssignee>()
            .eq("model_id", modelId)
            .eq("activity_id", activityId));
    }
}
