package com.luoyx.vjsb.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luoyx.vjsb.activiti.entity.TaskAssignee;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/2 9:24
 */
public interface TaskAssigneeMapper extends BaseMapper<TaskAssignee> {
}
