package com.luoyx.vjsb.activiti.entity;

import com.luoyx.vjsb.common.query.CommonQuery;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 10:40
 */
@Data
public class ProcessDefinition extends CommonQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 流程名称
     */
    private String name;

    /**
     * 流程KEY
     */
    private String key;

    /**
     * 流程版本
     */
    private int version;

    /**
     * 所属分类
     */
    private String category;

    /**
     * 流程描述
     */
    private String description;

    private String deploymentId;

    /**
     * 部署时间
     */
    private Date deploymentTime;

    /**
     * 流程图
     */
    private String diagramResourceName;

    /**
     * 流程定义
     */
    private String resourceName;

    /** 流程实例状态 1 激活 2 挂起 */
    private String suspendState;

    private String suspendStateName;
}
