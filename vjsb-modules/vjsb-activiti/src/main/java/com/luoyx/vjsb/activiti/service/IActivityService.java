package com.luoyx.vjsb.activiti.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luoyx.vjsb.activiti.vo.CurrentTaskVO;
import com.luoyx.vjsb.activiti.vo.ProcessInstanceVO;
import com.luoyx.vjsb.common.query.CommonQuery;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.repository.Model;
import org.activiti.rest.service.api.repository.ModelRequest;
import org.activiti.rest.service.api.repository.ModelResponse;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * <p>
 * 工作流
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 16:59
 */
public interface IActivityService {

    /**
     * 获取分页信息
     *
     * @param query 查询条件
     * @return 模型
     */
    Page<Model> getPage(CommonQuery query);

    /**
     * 创建模型
     *
     * @param name        模型名称
     * @param key         模型key
     * @param description 模型描述
     * @return 模型id
     */
    String createModeler(String name, String key, String description);

    /**
     * 修改模型
     *
     * @param id           模型id
     * @param modelRequest 模型实体类
     * @return 模型实体类
     */
    ModelResponse updateModel(String id, ModelRequest modelRequest);

    /**
     * 部署模型
     *
     * @param modelId 模型id
     * @return b
     */
    boolean deploy(String modelId);

    /**
     * 获取流程节点
     *
     * @param modelId 模型id
     * @return 流程节点
     * @throws IOException io
     */
    Collection<FlowElement> activityListFlowElement(String modelId) throws IOException;

    /**
     * 启动流程
     *
     * @param deployId 流程部署id
     * @param map      流程变量
     * @return ProcessInstanceVO
     */
    ProcessInstanceVO startProcess(String deployId, Map<String, Object> map);


    /**
     * 获取当前任务
     *
     * @param processInstanceId 流程实例id
     * @return CurrentTaskVO
     */
    CurrentTaskVO currentTask(String processInstanceId);


    /**
     * 分配任务
     *
     * @param taskId 任务id
     * @param roleId 用户id
     */
    void assignTask(String taskId, String roleId);

    /**
     * 完成任务
     *
     * @param taskId 任务id
     * @param map    流程变量
     */
    void completeTask(String taskId, Map<String, Object> map);

    /**
     * 设置下一个任务
     *
     * @param taskId 任务id
     * @return b
     */
    boolean nextTask(String taskId);

    /**
     * 结束流程
     *
     * @param processInstanceId 流程实例id
     * @param remark            备注
     */
    void finishProcessInstance(String processInstanceId, String remark);

    /**
     * 完成任务
     *
     * @param processInstanceId 流程id
     * @param dataMap           流程变量
     * @param state             审核状态 1-通过  0-不通过
     * @param remark            审核说明
     * @param taskId            任务id
     */
    void completeTaskWithPi(String processInstanceId, Map<String, Object> dataMap, Integer state, String remark, String taskId);

    /**
     * 添加任务额外数据（说明，结果）
     *
     * @param processInstanceId 流程实例id
     * @param taskId            任务id
     * @param result            结果
     * @param remark            说明
     * @param taskName          节点名称
     */
    void addAdditionalData(String processInstanceId, String taskId, Integer result, String remark, String taskName);

    /**
     * 启动流程 --第一个任务属于自己
     *
     * @param roleId  用户id
     * @param modelId 模型id
     * @param dataMap 流程变量
     * @param remark  说明
     * @return 流程实例id
     */
    ProcessInstanceVO firstTaskBelongToMe(String roleId, String modelId, Map<String, Object> dataMap, String remark);
}
