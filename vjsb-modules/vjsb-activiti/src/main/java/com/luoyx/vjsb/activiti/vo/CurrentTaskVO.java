package com.luoyx.vjsb.activiti.vo;

import lombok.Data;
import org.activiti.engine.task.Task;

/**
 * <p>
 * 当前任务实例vo
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/4 11:42
 */
@Data
public class CurrentTaskVO {

    /**
     * 当前任务
     */
    private Task task;

    /**
     * 是否是会签任务
     */
    private Boolean isSign;
}
