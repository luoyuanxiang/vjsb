package com.luoyx.vjsb.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luoyx.vjsb.activiti.entity.TaskAssignee;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/6/2 9:25
 */
public interface ITaskAssigneeService extends IService<TaskAssignee> {

    /**
     * 获取自定义委派人
     *
     * @param modelId 模型id
     * @param activityId 节点id
     * @return modelId
     */
    TaskAssignee queryByModelIdAndActivityId(String modelId, String activityId);
}
