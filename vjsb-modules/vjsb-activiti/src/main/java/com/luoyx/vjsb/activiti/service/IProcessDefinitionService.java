package com.luoyx.vjsb.activiti.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luoyx.vjsb.activiti.entity.ProcessDefinition;

import java.io.FileNotFoundException;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/5/15 10:45
 */
public interface IProcessDefinitionService {

    /**
     * 获取分页流程定义
     *
     * @param processDefinition 1
     * @return 1
     */
    Page<ProcessDefinition> listProcessDefinition(ProcessDefinition processDefinition);


    /**
     * 发布流程定义
     *
     * @param filePath 文件位置
     * @throws FileNotFoundException 1
     */
    void deployProcessDefinition(String filePath) throws FileNotFoundException;


    /**
     * 删除流程定义
     *
     * @param deploymentIds 流程id
     * @return 1
     * @throws Exception 1
     */
    int deleteProcessDeploymentByIds(String[] deploymentIds) throws Exception;


    /**
     * 激活或挂起
     * @param id 流程定义id
     * @param suspendState 1 挂起 2 激活
     */
    void suspendOrActiveApply(String id, Integer suspendState);
}
