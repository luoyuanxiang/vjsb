package com.luoyx.vjsb.common.constant;

/**
 * <p>
 * 系统常量
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/28 10:33
 */
public class ConstantCommon {

    /**
     * 超级管理员
     */
    public static final String SUPPER_ADMIN = "supper_admin";
}
