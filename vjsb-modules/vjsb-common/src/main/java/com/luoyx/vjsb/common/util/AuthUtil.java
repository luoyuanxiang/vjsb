package com.luoyx.vjsb.common.util;

import com.luoyx.vjsb.common.vo.SysUserVO;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户工具
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/3 17:53
 */
@Component
public class AuthUtil {

    /**
     * 用户信息
     */
    private final ThreadLocal<SysUserVO> user = new ThreadLocal<>();
    /**
     * token信息
     */
    private final ThreadLocal<String> token = new ThreadLocal<>();

    private final ThreadLocal<String> userName = new ThreadLocal<>();

    /**
     * 设置用户信息
     *
     * @param user 用户信息
     */
    public void setUser(SysUserVO user) {
        this.user.set(user);
    }

    /**
     * 获取用户信息
     *
     * @return data
     */
    public SysUserVO getUser() {
        return this.user.get();
    }

    public void removeUser() {
        this.user.remove();
    }

    /**
     * 设置token信息
     *
     * @param token token
     */
    public void setToken(String token) {
        this.token.set(token);
    }

    /**
     * 获取token
     *
     * @return token
     */
    public String getToken() {
        return this.token.get();
    }

    public void removeToken() {
        this.token.remove();
    }

    public void setUsername(String userName) {
        this.userName.set(userName);
    }
    public String getUsername() {
        return this.userName.get();
    }
    public void removeUsername() {
        this.userName.remove();
    }
}
