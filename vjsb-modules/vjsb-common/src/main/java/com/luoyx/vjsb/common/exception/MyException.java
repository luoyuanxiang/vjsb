package com.luoyx.vjsb.common.exception;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/4 16:56
 */
public class MyException extends RuntimeException{

    public MyException(String message) {
        super(message);
    }
}
