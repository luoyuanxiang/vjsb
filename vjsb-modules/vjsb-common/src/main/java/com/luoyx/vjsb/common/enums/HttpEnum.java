package com.luoyx.vjsb.common.enums;

import lombok.Getter;

/**
 * <p>
 *  http 返回状态码
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/20 15:55
 */
@Getter
public enum HttpEnum {

    /**
     * 状态码
     */
    OK(0, "成功"),
    FAIL(-1, "失败"),
    REFRESH_JWT(1005, "刷新token"),
    INVALID(1006, "token失效"),
    ERROR_JWT(1007, "jwt错误"),
    INVALID_REQUEST(1111, "请求无效");

    private int code;
    private String desc;
    HttpEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
