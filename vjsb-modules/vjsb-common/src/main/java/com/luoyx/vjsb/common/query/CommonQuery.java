package com.luoyx.vjsb.common.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 公共查询字段
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/25 14:05
 */
@Data
@ApiModel("查询封装")
public class CommonQuery {

    @ApiModelProperty( value = "每页显示条数，默认 10")
    private long size = 10;

    @ApiModelProperty( value = "当前页，默认 1")
    private long current = 1;

    /**
     *
     */
    @ApiModelProperty("搜索内容")
    private String searchText;
}
