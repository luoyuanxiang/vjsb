package com.luoyx.vjsb.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 自定义权限验证
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/4/30 11:54
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthPermissions {

    /**
     * 权限， 可以多个
     *
     * @return string
     */
    String value();

    /**
     * 角色，可以多个
     *
     * @return string
     */
    String role();

}
