package com.luoyx.vjsb.common.aspect;

import com.luoyx.vjsb.common.annotation.AuthPermissions;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * <p>
 * 自定义权限AOP
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/4/30 12:05
 */
@Component
@Aspect
public class AuthPermissionsAspect {

    @Pointcut("@annotation(com.luoyx.vjsb.common.annotation.AuthPermissions)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint pjp) {
        // 从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        // 获取切入点所在的方法
        Method method = signature.getMethod();
        AuthPermissions annotation = method.getAnnotation(AuthPermissions.class);
        // 权限
        String value = annotation.value();
        // 角色
        String role = annotation.role();
        try {
            Object proceed = pjp.proceed();
            return proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    private boolean hasPermissionAndRole(String permission, String role) {
        if (Objects.isNull(permission)) {
            return true;
        } else {
//            JwtAccount jwtAccount = JSONUtil.toBean(s, JwtAccount.class);
//            SysUser user = SpringContextHolder.getBean().getOne(new QueryWrapper<SysUser>().eq("username", jwtAccount.getSub()));
//            ShiroUtil.getPermissions(user);
        }
        if (Objects.isNull(role)) {
            return true;
        }
        return false;
    }
}
