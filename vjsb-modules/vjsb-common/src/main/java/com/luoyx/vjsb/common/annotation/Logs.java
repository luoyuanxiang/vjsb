package com.luoyx.vjsb.common.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * 日志
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/4/7 13:50
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Logs {

    String value() default "";
}
