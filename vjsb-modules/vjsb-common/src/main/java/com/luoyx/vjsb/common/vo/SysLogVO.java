package com.luoyx.vjsb.common.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 日志
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/3 18:20
 */
@Data
@Accessors(chain = true)
public class SysLogVO {

    /**
     * 登录用户id
     */
    private String userName;

    /**
     * 日志级别 ERROR','WARN','INFO
     */
    private String logLevel;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 日志内容
     */
    private String content;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 操作用户的user_agent
     */
    private String ua;

    /**
     * 请求方法
     */
    private String requestMethod;

    /**
     * 消耗时间
     */
    private Long consumingTime;

    /**
     * 异常详情信息
     */
    private String exDetail;

    /**
     * 异常描述
     */
    private String exMsg;

    private String id;

    private Date createTime;

}
