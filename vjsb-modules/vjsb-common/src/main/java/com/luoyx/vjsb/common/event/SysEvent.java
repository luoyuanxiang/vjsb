package com.luoyx.vjsb.common.event;

import com.luoyx.vjsb.common.vo.SysLogVO;
import org.springframework.context.ApplicationEvent;

/**
 * <p>
 * 日志监听
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/5/3 18:50
 */
public class SysEvent extends ApplicationEvent {
    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public SysEvent(SysLogVO source) {
        super(source);
    }
}
