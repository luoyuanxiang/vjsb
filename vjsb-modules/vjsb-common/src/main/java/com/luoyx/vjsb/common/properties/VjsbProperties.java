package com.luoyx.vjsb.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 自定义配置
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/3/23 10:00
 */
@Data
@Configuration
@ConfigurationProperties("vjsb")
public class VjsbProperties {

    /**
     * jwt过期时间 单位（秒） 默认 1小时
     */
    private long expire = 36000;

    /**
     * 过滤开关
     */
    private boolean enabled = true;

    /**
     * 不需要登录的地址
     */
    private List<String> defaultAnonUrls = new ArrayList<>();

    /**
     * 上传图片位置
     */
    private String fileUploadPathPrefix;

    /**
     * 图片映射地址
     */
    private String mappingImageUrl;
}
