package com.luoyx.vjsb.captcha.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *
 * </p>
 *
 * @author luoyuanxiang <p>luoyuanxiang.github.io</p>
 * @since 2020/4/2 17:45
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "vjsb.captcha")
public class CaptchaProperties {

    /**
     * GEETEST appId
     */
    private String geetestId;

    /**
     * GEETEST appKey
     */
    private String geetestKey;
    private Boolean newFailback;

    /**
     * 腾讯 appId
     */
    private String tencentAppId;

    /**
     * 腾讯 appKey
     */
    private String tencentAppSecret;
    private String tencentVerifyUri;

    /**
     * 腾讯验证
     */
    public static final String G_TYPE = "TencentCaptcha";

}
