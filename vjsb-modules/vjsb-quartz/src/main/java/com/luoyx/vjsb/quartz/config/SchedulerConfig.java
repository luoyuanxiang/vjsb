package com.luoyx.vjsb.quartz.config;

import org.quartz.Scheduler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.annotation.Resource;

/**
 * <p>
 * 任务调度
 * </p>
 *
 * @author luoyuanxiang
 * @date 2020/4/22 17:42
 */
@Configuration
public class SchedulerConfig {

    @Resource
    private SchedulerFactoryBean schedulerFactoryBean;

    /**
     * 通过SchedulerFactoryBean获取Scheduler的实例
     * @return s
     */
    @Bean(name="Scheduler")
    public Scheduler scheduler() {
        return schedulerFactoryBean.getScheduler();
    }
}
